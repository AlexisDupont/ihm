﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mastermind
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grp_2j = New System.Windows.Forms.GroupBox()
        Me.lbl_nbmanches = New System.Windows.Forms.Label()
        Me.nud_nbmanche = New System.Windows.Forms.NumericUpDown()
        Me.txt_nomj2 = New System.Windows.Forms.TextBox()
        Me.txt_nomj1 = New System.Windows.Forms.TextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btn_2jdécod = New System.Windows.Forms.RadioButton()
        Me.btn_2jcod = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btn_1jdécod = New System.Windows.Forms.RadioButton()
        Me.btn_1jcod = New System.Windows.Forms.RadioButton()
        Me.lbl_jouerMM = New System.Windows.Forms.Label()
        Me.btn_jouer = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_SMM = New System.Windows.Forms.RadioButton()
        Me.btn_MM = New System.Windows.Forms.RadioButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btn_1j = New System.Windows.Forms.RadioButton()
        Me.btn_2j = New System.Windows.Forms.RadioButton()
        Me.tlp_commentaire = New System.Windows.Forms.ToolTip(Me.components)
        Me.tlp_commentaire2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btn_Compter = New System.Windows.Forms.Button()
        Me.grp_2j.SuspendLayout()
        CType(Me.nud_nbmanche, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'grp_2j
        '
        Me.grp_2j.Controls.Add(Me.lbl_nbmanches)
        Me.grp_2j.Controls.Add(Me.nud_nbmanche)
        Me.grp_2j.Controls.Add(Me.txt_nomj2)
        Me.grp_2j.Controls.Add(Me.txt_nomj1)
        Me.grp_2j.Controls.Add(Me.GroupBox5)
        Me.grp_2j.Controls.Add(Me.GroupBox4)
        Me.grp_2j.Location = New System.Drawing.Point(234, 251)
        Me.grp_2j.Name = "grp_2j"
        Me.grp_2j.Size = New System.Drawing.Size(407, 194)
        Me.grp_2j.TabIndex = 4
        Me.grp_2j.TabStop = False
        Me.grp_2j.Text = "2 joueurs"
        '
        'lbl_nbmanches
        '
        Me.lbl_nbmanches.AutoSize = True
        Me.lbl_nbmanches.Location = New System.Drawing.Point(18, 146)
        Me.lbl_nbmanches.Name = "lbl_nbmanches"
        Me.lbl_nbmanches.Size = New System.Drawing.Size(103, 13)
        Me.lbl_nbmanches.TabIndex = 5
        Me.lbl_nbmanches.Text = "nombre de manches"
        '
        'nud_nbmanche
        '
        Me.nud_nbmanche.Location = New System.Drawing.Point(264, 144)
        Me.nud_nbmanche.Name = "nud_nbmanche"
        Me.nud_nbmanche.Size = New System.Drawing.Size(120, 20)
        Me.nud_nbmanche.TabIndex = 4
        Me.nud_nbmanche.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nud_nbmanche.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'txt_nomj2
        '
        Me.txt_nomj2.Location = New System.Drawing.Point(18, 95)
        Me.txt_nomj2.Name = "txt_nomj2"
        Me.txt_nomj2.Size = New System.Drawing.Size(100, 20)
        Me.txt_nomj2.TabIndex = 3
        Me.txt_nomj2.Text = "nom du joueur 2"
        Me.tlp_commentaire2.SetToolTip(Me.txt_nomj2, "Saisissez votre nom")
        '
        'txt_nomj1
        '
        Me.txt_nomj1.Location = New System.Drawing.Point(18, 39)
        Me.txt_nomj1.Name = "txt_nomj1"
        Me.txt_nomj1.Size = New System.Drawing.Size(100, 20)
        Me.txt_nomj1.TabIndex = 2
        Me.txt_nomj1.Text = "nom du joueur 1"
        Me.tlp_commentaire.SetToolTip(Me.txt_nomj1, "Saisissez votre nom")
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btn_2jdécod)
        Me.GroupBox5.Controls.Add(Me.btn_2jcod)
        Me.GroupBox5.Location = New System.Drawing.Point(187, 75)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(197, 50)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        '
        'btn_2jdécod
        '
        Me.btn_2jdécod.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_2jdécod.AutoSize = True
        Me.btn_2jdécod.Location = New System.Drawing.Point(103, 20)
        Me.btn_2jdécod.Name = "btn_2jdécod"
        Me.btn_2jdécod.Size = New System.Drawing.Size(62, 23)
        Me.btn_2jdécod.TabIndex = 1
        Me.btn_2jdécod.Text = "décodeur"
        Me.btn_2jdécod.UseMnemonic = False
        Me.btn_2jdécod.UseVisualStyleBackColor = True
        '
        'btn_2jcod
        '
        Me.btn_2jcod.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_2jcod.AutoSize = True
        Me.btn_2jcod.Location = New System.Drawing.Point(19, 19)
        Me.btn_2jcod.Name = "btn_2jcod"
        Me.btn_2jcod.Size = New System.Drawing.Size(50, 23)
        Me.btn_2jcod.TabIndex = 0
        Me.btn_2jcod.Text = "codeur"
        Me.btn_2jcod.UseMnemonic = False
        Me.btn_2jcod.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.btn_1jdécod)
        Me.GroupBox4.Controls.Add(Me.btn_1jcod)
        Me.GroupBox4.Location = New System.Drawing.Point(187, 19)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(197, 50)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'btn_1jdécod
        '
        Me.btn_1jdécod.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_1jdécod.AutoSize = True
        Me.btn_1jdécod.Location = New System.Drawing.Point(103, 20)
        Me.btn_1jdécod.Name = "btn_1jdécod"
        Me.btn_1jdécod.Size = New System.Drawing.Size(62, 23)
        Me.btn_1jdécod.TabIndex = 1
        Me.btn_1jdécod.Text = "décodeur"
        Me.btn_1jdécod.UseMnemonic = False
        Me.btn_1jdécod.UseVisualStyleBackColor = True
        '
        'btn_1jcod
        '
        Me.btn_1jcod.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_1jcod.AutoSize = True
        Me.btn_1jcod.Location = New System.Drawing.Point(19, 20)
        Me.btn_1jcod.Name = "btn_1jcod"
        Me.btn_1jcod.Size = New System.Drawing.Size(50, 23)
        Me.btn_1jcod.TabIndex = 0
        Me.btn_1jcod.Text = "codeur"
        Me.btn_1jcod.UseMnemonic = False
        Me.btn_1jcod.UseVisualStyleBackColor = True
        '
        'lbl_jouerMM
        '
        Me.lbl_jouerMM.AutoSize = True
        Me.lbl_jouerMM.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.25!)
        Me.lbl_jouerMM.Location = New System.Drawing.Point(320, 39)
        Me.lbl_jouerMM.Name = "lbl_jouerMM"
        Me.lbl_jouerMM.Size = New System.Drawing.Size(216, 26)
        Me.lbl_jouerMM.TabIndex = 5
        Me.lbl_jouerMM.Text = "Jouer au Mastermind"
        '
        'btn_jouer
        '
        Me.btn_jouer.Location = New System.Drawing.Point(325, 481)
        Me.btn_jouer.Name = "btn_jouer"
        Me.btn_jouer.Size = New System.Drawing.Size(211, 23)
        Me.btn_jouer.TabIndex = 6
        Me.btn_jouer.Text = "Jouer"
        Me.btn_jouer.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_SMM)
        Me.Panel1.Controls.Add(Me.btn_MM)
        Me.Panel1.Location = New System.Drawing.Point(325, 92)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(217, 68)
        Me.Panel1.TabIndex = 7
        '
        'btn_SMM
        '
        Me.btn_SMM.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_SMM.AutoSize = True
        Me.btn_SMM.Location = New System.Drawing.Point(111, 23)
        Me.btn_SMM.Name = "btn_SMM"
        Me.btn_SMM.Size = New System.Drawing.Size(99, 23)
        Me.btn_SMM.TabIndex = 2
        Me.btn_SMM.TabStop = True
        Me.btn_SMM.Text = "SuperMastermind"
        Me.btn_SMM.UseVisualStyleBackColor = True
        '
        'btn_MM
        '
        Me.btn_MM.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_MM.AutoSize = True
        Me.btn_MM.Location = New System.Drawing.Point(11, 23)
        Me.btn_MM.Name = "btn_MM"
        Me.btn_MM.Size = New System.Drawing.Size(71, 23)
        Me.btn_MM.TabIndex = 1
        Me.btn_MM.TabStop = True
        Me.btn_MM.Text = "Mastermind"
        Me.btn_MM.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btn_1j)
        Me.Panel2.Controls.Add(Me.btn_2j)
        Me.Panel2.Location = New System.Drawing.Point(325, 184)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(217, 61)
        Me.Panel2.TabIndex = 8
        '
        'btn_1j
        '
        Me.btn_1j.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_1j.AutoSize = True
        Me.btn_1j.Location = New System.Drawing.Point(18, 19)
        Me.btn_1j.Name = "btn_1j"
        Me.btn_1j.Size = New System.Drawing.Size(55, 23)
        Me.btn_1j.TabIndex = 4
        Me.btn_1j.TabStop = True
        Me.btn_1j.Text = "1 joueur"
        Me.btn_1j.UseVisualStyleBackColor = True
        '
        'btn_2j
        '
        Me.btn_2j.Appearance = System.Windows.Forms.Appearance.Button
        Me.btn_2j.AutoSize = True
        Me.btn_2j.Location = New System.Drawing.Point(130, 19)
        Me.btn_2j.Name = "btn_2j"
        Me.btn_2j.Size = New System.Drawing.Size(60, 23)
        Me.btn_2j.TabIndex = 3
        Me.btn_2j.TabStop = True
        Me.btn_2j.Text = "2 joueurs"
        Me.btn_2j.UseVisualStyleBackColor = True
        '
        'tlp_commentaire
        '
        Me.tlp_commentaire.IsBalloon = True
        Me.tlp_commentaire.ToolTipTitle = "Nom du joueur 1"
        '
        'tlp_commentaire2
        '
        Me.tlp_commentaire2.IsBalloon = True
        Me.tlp_commentaire2.ToolTipTitle = "Nom du joueur 2"
        '
        'btn_Compter
        '
        Me.btn_Compter.Location = New System.Drawing.Point(325, 452)
        Me.btn_Compter.Name = "btn_Compter"
        Me.btn_Compter.Size = New System.Drawing.Size(210, 23)
        Me.btn_Compter.TabIndex = 9
        Me.btn_Compter.Text = "Compter"
        Me.btn_Compter.UseVisualStyleBackColor = True
        '
        'Mastermind
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(886, 545)
        Me.Controls.Add(Me.btn_Compter)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btn_jouer)
        Me.Controls.Add(Me.lbl_jouerMM)
        Me.Controls.Add(Me.grp_2j)
        Me.Name = "Mastermind"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mastermind"
        Me.grp_2j.ResumeLayout(False)
        Me.grp_2j.PerformLayout()
        CType(Me.nud_nbmanche, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grp_2j As GroupBox
    Friend WithEvents lbl_nbmanches As Label
    Friend WithEvents nud_nbmanche As NumericUpDown
    Friend WithEvents txt_nomj2 As TextBox
    Friend WithEvents txt_nomj1 As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents btn_2jdécod As RadioButton
    Friend WithEvents btn_2jcod As RadioButton
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents btn_1jdécod As RadioButton
    Friend WithEvents btn_1jcod As RadioButton
    Friend WithEvents lbl_jouerMM As Label
    Friend WithEvents btn_jouer As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btn_SMM As RadioButton
    Friend WithEvents btn_MM As RadioButton
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btn_1j As RadioButton
    Friend WithEvents btn_2j As RadioButton
    Friend WithEvents tlp_commentaire As ToolTip
    Friend WithEvents tlp_commentaire2 As ToolTip
    Friend WithEvents btn_Compter As Button
End Class
