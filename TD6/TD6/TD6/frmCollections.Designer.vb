﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCollections
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstBox1 = New System.Windows.Forms.ListBox()
        Me.txtBox = New System.Windows.Forms.TextBox()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lstBox1
        '
        Me.lstBox1.FormattingEnabled = True
        Me.lstBox1.Location = New System.Drawing.Point(324, 224)
        Me.lstBox1.Name = "lstBox1"
        Me.lstBox1.Size = New System.Drawing.Size(120, 95)
        Me.lstBox1.TabIndex = 0
        '
        'txtBox
        '
        Me.txtBox.Location = New System.Drawing.Point(336, 133)
        Me.txtBox.Name = "txtBox"
        Me.txtBox.Size = New System.Drawing.Size(100, 20)
        Me.txtBox.TabIndex = 1
        '
        'btn2
        '
        Me.btn2.Location = New System.Drawing.Point(503, 368)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(75, 23)
        Me.btn2.TabIndex = 2
        Me.btn2.Text = "Bouton2"
        Me.btn2.UseVisualStyleBackColor = True
        '
        'btn1
        '
        Me.btn1.Location = New System.Drawing.Point(237, 367)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(75, 23)
        Me.btn1.TabIndex = 3
        Me.btn1.Text = "Bouton1"
        Me.btn1.UseVisualStyleBackColor = True
        '
        'frmCollections
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.txtBox)
        Me.Controls.Add(Me.lstBox1)
        Me.Name = "frmCollections"
        Me.Text = "frmCollections"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstBox1 As ListBox
    Friend WithEvents txtBox As TextBox
    Friend WithEvents btn2 As Button
    Friend WithEvents btn1 As Button
End Class
