﻿Public Class frmJeu



    Private Sub CmdT_Click(sender As Object, e As EventArgs) Handles cmdT1.Click, cmdT9.Click, cmdT8.Click, cmdT7.Click, cmdT6.Click, cmdT5.Click, cmdT4.Click, cmdT3.Click, cmdT2.Click, cmdT10.Click
        For i As Byte = 2 To 10
            Me.Controls("grpt" & i).Enabled = False
        Next
    End Sub
    Private Sub frmExo2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim pic As PictureBox
        Dim mongrp As GroupBox

        For i As Byte = 2 To 10
            Me.Controls("grpt" & i).Enabled = False
        Next
        For tentative1 As Byte = 1 To 10
            mongrp = Me.Controls("grpT" & tentative1)
            mongrp.Enabled = False
            For tentative2 As Byte = 1 To 4
                pic = mongrp.Controls("pict" & tentative1 & tentative2)
                pic.AllowDrop = True
                AddHandler pic.DragEnter, AddressOf pict_DragEnter
                AddHandler pic.DragDrop, AddressOf pict_DragDrop

            Next
        Next
        grpT1.Enabled = True
    End Sub
    Private Sub PicCol_MouseMove(sender As Object, e As MouseEventArgs) Handles picCol1.MouseMove, picCol2.MouseMove, picCol3.MouseMove, picCol4.MouseMove, picCol5.MouseMove, picCol6.MouseMove, picColR.MouseMove
        Dim pic As PictureBox = sender
        If e.Button = MouseButtons.Left Then
            pic.DoDragDrop(pic.BackColor, DragDropEffects.Copy)
        End If
    End Sub
    Private Sub pict_DragDrop(sender As Object, e As DragEventArgs)
        Dim pict As PictureBox = sender
        pict.BackColor = e.Data.GetData(pict.BackColor.GetType.ToString)

    End Sub

    Private Sub pict_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent("System.Drawing.Color") = True Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

End Class