﻿Public Class frmExo2
    Private Sub txt_Dp_MouseMove(sender As Object, e As MouseEventArgs) Handles txt_Dp.MouseMove, txt_Dst.MouseMove
        Dim txtDep As TextBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left Then
            rep = txtDep.DoDragDrop(txtDep.Text, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                txtDep.Clear()
            End If

        End If
    End Sub

    Private Sub frmExo2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txt_Dst.AllowDrop = True
        txt_Dp.AllowDrop = True
        PbDst.AllowDrop = True
        PbDp.AllowDrop = True


    End Sub

    Private Sub txt_Dst_DragDrop(sender As Object, e As DragEventArgs) Handles txt_Dst.DragDrop, txt_Dp.DragDrop
        Dim txtDest As TextBox = sender
        txtDest.Text = e.Data.GetData(DataFormats.Text)
    End Sub

    Private Sub txt_Dst_DragEnter(sender As Object, e As DragEventArgs) Handles txt_Dst.DragEnter, txt_Dp.DragEnter
        If e.Data.GetDataPresent(DataFormats.Text) = True Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub PbDp_MouseMove(sender As Object, e As MouseEventArgs) Handles PbDp.MouseMove, PbDst.MouseMove
        Dim PbDp As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left Then
            rep = PbDp.DoDragDrop(PbDp.Image, DragDropEffects.Move)
            If (rep = DragDropEffects.Move) Then
                PbDp.Image = Nothing
            End If

        End If
    End Sub

    Private Sub PbDst_DragDrop(sender As Object, e As DragEventArgs) Handles PbDst.DragDrop, PbDp.DragDrop
        Dim PbDst As PictureBox = sender
        PbDst.Image = e.Data.GetData(DataFormats.Bitmap)

    End Sub

    Private Sub PbDst_DragEnter(sender As Object, e As DragEventArgs) Handles PbDst.DragEnter, PbDp.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) = True Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
End Class

