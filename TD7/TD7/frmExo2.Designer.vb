﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExo2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExo2))
        Me.txt_Dp = New System.Windows.Forms.TextBox()
        Me.txt_Dst = New System.Windows.Forms.TextBox()
        Me.PbDp = New System.Windows.Forms.PictureBox()
        Me.PbDst = New System.Windows.Forms.PictureBox()
        CType(Me.PbDp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PbDst, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_Dp
        '
        Me.txt_Dp.Location = New System.Drawing.Point(57, 45)
        Me.txt_Dp.Name = "txt_Dp"
        Me.txt_Dp.Size = New System.Drawing.Size(100, 20)
        Me.txt_Dp.TabIndex = 0
        '
        'txt_Dst
        '
        Me.txt_Dst.Location = New System.Drawing.Point(340, 45)
        Me.txt_Dst.Name = "txt_Dst"
        Me.txt_Dst.Size = New System.Drawing.Size(100, 20)
        Me.txt_Dst.TabIndex = 1
        '
        'PbDp
        '
        Me.PbDp.Image = CType(resources.GetObject("PbDp.Image"), System.Drawing.Image)
        Me.PbDp.Location = New System.Drawing.Point(57, 101)
        Me.PbDp.Name = "PbDp"
        Me.PbDp.Size = New System.Drawing.Size(255, 229)
        Me.PbDp.TabIndex = 2
        Me.PbDp.TabStop = False
        '
        'PbDst
        '
        Me.PbDst.Location = New System.Drawing.Point(340, 101)
        Me.PbDst.Name = "PbDst"
        Me.PbDst.Size = New System.Drawing.Size(239, 229)
        Me.PbDst.TabIndex = 3
        Me.PbDst.TabStop = False
        '
        'frmExo2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(608, 378)
        Me.Controls.Add(Me.PbDst)
        Me.Controls.Add(Me.PbDp)
        Me.Controls.Add(Me.txt_Dst)
        Me.Controls.Add(Me.txt_Dp)
        Me.Name = "frmExo2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Exo2"
        CType(Me.PbDp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PbDst, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_Dp As TextBox
    Friend WithEvents txt_Dst As TextBox
    Friend WithEvents PbDp As PictureBox
    Friend WithEvents PbDst As PictureBox
End Class
